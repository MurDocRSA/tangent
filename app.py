# pravin.gordhan

from flask import Flask, flash, redirect, render_template, request, session, abort, url_for, Response
import requests
import os
import json


app = Flask(__name__)
app.secret_key = 'tangent'


@app.route('/')
def home():
    if not session.get('token'):
        # return render_template('login.html')
        return redirect(url_for('login'))
    else:
        return render_template("template.html")

# Route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':

        params = {
            'username': request.form['username'],
            'password': request.form['password']
        }

        r = requests.post('http://staging.tangent.tngnt.co/api-token-auth/', params)

        if r.status_code == 200:
            session['token'] = r.json()['token']
            return redirect(url_for('home'))
        else:
            session['token'] = None
            error = 'Invalid Credentials. Please try again.'

    return render_template('login.html', error=error)


@app.route('/logout')
def logout():
   # remove the token from the session if it is there
   session.pop('token', None)
   return redirect(url_for('home'))


@app.route('/dashboard')
def dashboard():
    return render_template("dashboard.html")


@app.route('/employees')
def employees():
    headers = {'Authorization': 'Token ' + session.get('token')}
    r = requests.get('http://staging.tangent.tngnt.co/api/employee/', headers=headers)

    if r.status_code == 200:
        success = True
        json_string = r.text
        obj = json.loads(json_string)
    else:
        success = False

    return render_template("employees.html", status=success, data=obj)


@app.route('/statistics')
def statistics():
    headers = {'Authorization': 'Token ' + session.get('token')}
    r = requests.get('http://staging.tangent.tngnt.co/api/employee/', headers=headers)

    if r.status_code == 200:
        success = True
        json_string = r.text
        obj = json.loads(json_string)
    else:
        success = False

    return render_template("myprofile.html", status=success, data=obj)

@app.route('/myprofile')
def myprofile():
    headers = {'Authorization': 'Token ' + session.get('token')}
    r = requests.get('http://staging.tangent.tngnt.co/api/user/me', headers=headers)

    if r.status_code == 200:
        success = True
        json_string = r.text
        obj = json.loads(json_string)
    else:
        success = False

    return render_template("myprofile.html", status=success, data=obj)


if __name__ == '__main__':
    app.run(debug=True)
